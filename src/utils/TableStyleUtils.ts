import {isToday, isWeekend} from "date-fns";
import React from "react";

/**
 * Determines the background color for a given date based on whether it is a weekend or not.
 * @param date - The date to check.
 * @returns The background color class name for the date.
 */
export const getDayColor = (date: Date) => {
    return isWeekend(date) ?
        // weekend
        (isToday(date) ? "bg-emerald-100" : "bg-gray-100") :
        // workday
        (isToday(date) ? "bg-emerald-100" : "");
}

const crossingColor = "bg-blue-100";

const hoverPlusIconAdd = (cell: HTMLTableCellElement) => {
    const targetCellChildren = cell.children[0].children;
    const hoverHideClass = "hidden";

    if(targetCellChildren){
        for (let i = 0; i < targetCellChildren.length; i++) {
            if(targetCellChildren[i].classList.contains(hoverHideClass)){
                targetCellChildren[i].classList.remove(hoverHideClass);
            }else{
                targetCellChildren[i].classList.add(hoverHideClass);
            }
        }
    }
}

export const crossingColorChangeAdd = (event: React.MouseEvent<HTMLTableCellElement>) => {
    const targetCell = event.currentTarget;
    const rowIndex = targetCell.cellIndex;
    const tableBody = targetCell.closest("tbody");
    const rows = tableBody?.getElementsByTagName("tr");
    const rowCells = targetCell.parentElement?.children;

    hoverPlusIconAdd(targetCell);

    if (rowCells) {
        for (let i = 0; i < rowCells.length - 1; i++) {
                rowCells[i].classList.add(crossingColor);
            }
    }
    if (rows) {
        for (let i = 0; i < rows.length; i++) {
            let cells = rows[i].getElementsByTagName("td");
            if (cells.length > rowIndex) {
                cells[rowIndex].classList.add(crossingColor);
            }

        }
    }
};

export const crossingColorChangeRemove = (event: React.MouseEvent<HTMLTableCellElement>) => {
    const targetCell = event.currentTarget;
    const rowIndex = targetCell.cellIndex;
    const tableBody = targetCell.closest("tbody");
    const rows = tableBody?.getElementsByTagName("tr");
    const rowCells = targetCell.parentElement?.children;

    hoverPlusIconAdd(targetCell);

    if (rowCells) {
        for (let i = 0; i < rowCells.length - 1; i++) {
                rowCells[i].classList.remove(crossingColor);
        }
    }
    if (rows) {
        for (let i = 0; i < rows.length; i++) {
            let cells = rows[i].getElementsByTagName("td");
            if (cells.length > rowIndex) {
                cells[rowIndex].classList.remove(crossingColor);
            }

        }
    }
}
