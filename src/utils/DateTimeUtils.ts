import {endOfMonth, endOfWeek, format, isToday, isWeekend, setDate, startOfMonth, startOfWeek} from 'date-fns';

/**
 * Formats the given number of seconds into a time format (HH:mm).
 * @param seconds - The number of seconds to format.
 * @returns The formatted time in the format HH:mm.
 */
export const formatTime = (seconds: number) => {

    const hours = Math.floor(seconds / 3600);
    const mins = (seconds % 3600)/3600*60;
    return `${hours.toString().padStart(2, '0')}:${mins.toString().padStart(2, '0')}`;
};

/**
 * Formats the day and month into a displayable format.
 * @param day - The day of the month.
 * @param month - The month to which the day belongs.
 * @returns The formatted day and month.
 */
export const formatDay = (day: number, month: Date) => {
    const date = setDate(month, day);
    return format(date, 'do E');
}

/**
 * Calculates the number of man-days based on the given time in seconds.
 * @param time - The time in seconds.
 * @returns The calculated number of man-days.
 */
export const getManDays = (time: number) => {
    return (time / 28800).toFixed(2);
}

export const getMonthPeriod = (month: Date) => {
    return format(startOfMonth(month), 'dd/MM/yyyy') + " - " +
            format(endOfMonth(month), 'dd/MM/yyyy');
}

export const getWeekPeriod = (month: Date) => {
    return format(startOfWeek(month), 'dd/MM/yyyy', {weekStartsOn: 1}) + " - " +
            format(endOfWeek(month), 'dd/MM/yyyy', {weekStartsOn: 1});
}