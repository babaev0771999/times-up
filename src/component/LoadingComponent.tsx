import React from 'react';
import { AiOutlineLoading } from 'react-icons/ai';

function LoadingComponent () {
    return (
        <div className="flex items-center justify-center h-96">
            <AiOutlineLoading className="animate-spin text-4xl" />
            <span className="ml-2">Loading...</span>
        </div>
    );
}

export default LoadingComponent;
