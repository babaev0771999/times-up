import React, {useEffect, useState} from 'react';
import {addMonths, format, getDaysInMonth, getMonth, getYear} from 'date-fns';
import {formatDay, formatTime, getManDays, getMonthPeriod} from "../utils/DateTimeUtils"
import {useIssuesData} from "../hooks/issueData";
import {crossingColorChangeAdd, crossingColorChangeRemove, getDayColor} from "../utils/TableStyleUtils";
import {AddTimeModal} from "./AddTimeModal";
import {Issue, Timelog, User} from "../gql model/graphql";
import {AiOutlineArrowLeft, AiOutlineArrowRight} from "react-icons/ai"
import {Navigation} from "./Navigation";
import {useQuery} from "@apollo/client";
import {LOGIN} from "../services/queries";
import {Navigate} from "react-router-dom";
import {BsPlusLg} from "react-icons/bs"
import {LogsTableModal} from "./LogsTableModal";
import LoadingComponent from "./LoadingComponent";

function TimesheetTable() {

    const {data: loginData, loading: LoginLoading} = useQuery(LOGIN);
    const [currentDate, setCurrentDate] = useState(new Date());
    const daysInMonth = getDaysInMonth(currentDate);
    const daysArray = Array.from({length: daysInMonth}, (_, i) => i + 1);
    const {projects, issues, timelogs, currentUser, error, loading: issuesLoading} = useIssuesData();
    const [isAddModalOpen, setAddModalOpen] = useState(false);
    const [isDeleteModalOpen, setDeleteModalOpen] = useState(false);
    const [chosenDate, setChosenDate] = useState(new Date());
    const [chosenIssue, setChosenIssue] = useState<Issue | null>(null);

    useEffect(() => {
        // Проверяем, загружены ли данные issues
        if (!issuesLoading && issues.length > 0) {
            setChosenIssue(issues[0]);
        }
    }, [issues, issuesLoading]);

    //-----Style vars-----//
    const monthButtonStyle = "px-4 py-2 m-3 rounded border h-10";
    const actionButtonStyle = monthButtonStyle + " bg-blue-500 text-white";

    const openAddModal = () => {
        setAddModalOpen(true);
    };

    const closeAddModal = () => {
        setAddModalOpen(false);
    };

    const openDeleteModal = () => {
        setDeleteModalOpen(true);
    };

    const closeDeleteModal = () => {
        setDeleteModalOpen(false);
    };

    /**
     * Sets data for the timelog, updating the chosen issue and date.
     * @param issue - The issue for which the timelog data is being set.
     * @param day - The day for which the timelog data is being set.
     */
    const setDataForTimelog = (issue: Issue, day: number) => {
        setChosenIssue(issue);
        setChosenDate(new Date(currentDate.getFullYear(), currentDate.getMonth(), day));
    }

    /**
     * Calculates the total time spent on a specific issue.
     * @param issueId - The ID of the issue.
     * @returns The formatted total time spent on the issue.
     */
    const getTotalTimeForIssue = (issueId: string) => {

        const totalTime = timelogs.filter((entry) => entry.issue?.id === issueId &&
            getMonth(new Date(entry.spentAt)) === getMonth(currentDate) &&
            getYear(new Date(entry.spentAt)) === getYear(currentDate))
            .reduce((sum, entry) => sum + entry.timeSpent, 0);

        return formatTime(totalTime);
    };

    /**
     * Calculates the total time spent in hours for a specific day across all issues.
     * @param day - The day of the month (1-31).
     * @returns The formatted total time spent in hours for the specified day.
     */
    const getTotalTimeForDay = (day: number) => {
        const totalTime = timelogs.filter((entry) =>
            format(new Date(entry.spentAt), "dd/MM/yyyy") === format(new Date(currentDate.getFullYear(), currentDate.getMonth(), day), "dd/MM/yyyy"))
            .reduce((sum, entry) => sum + entry.timeSpent, 0);
        return formatTime(totalTime);
    };

    /**
     * Calculates the total time spent in hours for the current month across all issues.
     * @returns The formatted total time spent in hours for the month.
     */
    const getTotalTimeForMonth = () => {
        return timelogs.filter(entry => getMonth(new Date(entry.spentAt)) === getMonth(currentDate) &&
            getYear(new Date(entry.spentAt)) === getYear(currentDate))
            .map(entry => entry.timeSpent).reduce((sum, time) => sum + time, 0);
    }


    /**
     * Calculates the total time spent in man-days for a specific month.
     * @returns The total time spent in man-days for the month.
     */
    const getTotalTimeForMonthInManDays = () => {
        return getManDays(getTotalTimeForMonth());
    }

    const filterIssuesForTable = () => {
        return issues.filter(issue => issue.timelogs?.nodes &&
            issue.timelogs.nodes.length > 0 &&
            issue.timelogs.nodes.some(timelog => format(new Date(timelog?.spentAt), "MM/yyyy") === format(currentDate, "MM/yyyy"))
        )
    }


    /**
     * Switches the month by adding or subtracting a specified amount of months.
     * @param amount - The number of months to add or subtract. Use a positive number to move forward in time and a negative number to move backward.
     */
    const switchMonth = (amount: number) => {
        const result = addMonths(currentDate, amount);
        setCurrentDate(result);
    }


    /**
     * Retrieves the time spent on a specific issue for a given day and month.
     * @param issueId - The ID of the issue.
     * @param day - The day of the month (1-31).
     * @param month - The month for which to retrieve the time entries.
     * @returns The formatted time spent on the issue, or an empty string if no entry is found.
     */
    const getTimeForIssueAndDay = (issueId: string, day: number, month: Date) => {
        const entries = timelogs.filter(
            (entry) => entry.issue?.id === issueId &&
                format(new Date(entry.spentAt), 'd') === day.toString() &&
                getMonth(new Date(entry.spentAt)) === getMonth(month) &&
                getYear(new Date(entry.spentAt)) === getYear(month));
        if (entries.length > 0) {
            // Если есть записи, преобразуйте их и верните как массив строк
            const sum = entries.map(entry => entry.timeSpent)
                .reduce((accumulator, currentValue) => accumulator + currentValue, 0);
            return formatTime(sum)
        } else {
            // Если нет подходящих записей, верните пустой массив или другое значение по умолчанию
            return '';
        }
    };

    /**
     * Generates a CSV file based on the table data and initiates its download.
     */
    const downloadTableAsCSV = () => {
        const csvContent = createCSVContent();
        const encodedCSVContent = encodeURIComponent(csvContent);
        const downloadLink = document.createElement('a');
        downloadLink.href = `data:text/csv;charset=utf-8,${encodedCSVContent}`;
        downloadLink.download = `${format(currentDate, "MMMM").toLowerCase()}_timesheet.csv`;
        downloadLink.click();
    };

    /**
     * Generates the content for a CSV file based on the issue's data.
     * @returns The CSV content as a string.
     */
    const createCSVContent = () => {
        let csvContent = '';

        // Add table headers
        csvContent += 'Issue,';
        for (let day = 1; day <= daysInMonth; day++) {
            const formattedDay = formatDay(day, currentDate);
            csvContent += `"${formattedDay}",`;
        }
        csvContent += 'Total\n';

        // Add table rows
        for (const issue of issues) {
            csvContent += `"${issue.title}",`;
            for (let day = 1; day <= daysInMonth; day++) {
                const time = getTimeForIssueAndDay(issue.id, day, currentDate);
                csvContent += `"${time}",`;
            }
            const totalTime = getTotalTimeForIssue(issue.id);
            csvContent += `"${totalTime}"\n`;
        }

        // Add total man-days row
        const totalManDays = getTotalTimeForMonthInManDays();
        csvContent += `MND: ${totalManDays}\n`;

        return csvContent;
    };


    if (error) {
        return (
            <h1>Error is occurred: ${error}</h1>
        )
    }

    if (!LoginLoading && !loginData.currentUser) {
        return <Navigate replace to="/login"/>;
    }

    return (
        <div>
            <Navigation/>
            <div className="flex justify-between">
                <div className="sticky left-0">
                    <button onClick={() => setCurrentDate(new Date())} className={monthButtonStyle}>Today
                    </button>
                    <button onClick={() => switchMonth(-1)} className={monthButtonStyle}>
                        <AiOutlineArrowLeft/>
                    </button>
                    <span className="px-4 py-2 rounded border whitespace-nowrap">
                        {getMonthPeriod(currentDate)}
                    </span>
                    <button onClick={() => switchMonth(1)} className={monthButtonStyle}>
                        <AiOutlineArrowRight/>
                    </button>
                </div>
                <div className="sticky right-0">
                    <button onClick={() => {
                        setDataForTimelog(issues[0], new Date().getDate());
                        openAddModal();
                    }}
                            className={actionButtonStyle}
                            disabled={issuesLoading}>
                        Add Timelog
                    </button>
                </div>
            </div>
            {!issuesLoading ? (
                <div className="overflow-x-auto flex">
                    <table className="border-collapse w-full text-xs">
                        <thead className="border">
                        <tr className="bg-gray-200">
                            <th className="sticky left-0 bg-gray-200 px-4 py-2 font-normal">Issue</th>
                            {daysArray.map((day) => (
                                <th key={day} className="px-4 py-2 font-normal w-8">
                                    {formatDay(day, currentDate)}
                                </th>
                            ))}
                            <th className="px-4 py-2">Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr className="text-center">
                            <td className="sticky left-0 bg-white border font-bold px-4 py-2 whitespace-nowrap">Daily
                                Sum:
                            </td>
                            {daysArray.map((day) => (
                                <td key={'sumPerDay' + day}
                                    className={"border p-0  " + getDayColor(new Date(currentDate.getFullYear(), currentDate.getMonth(), day))}>
                                    <div className={"flex justify-center items-center"}>
                                        <span>
                                           {getTotalTimeForDay(day)}
                                        </span>
                                        <BsPlusLg className={"hidden"}/>
                                    </div>
                                </td>
                            ))}
                        </tr>
                        {filterIssuesForTable().map((issue) => (
                            <tr key={issue.id} className="text-center">
                                <td className="sticky left-0 bg-white border px-4 py-2 whitespace-nowrap">{issue.title}</td>
                                {daysArray.map((day) => (
                                    <td key={day}
                                        className={"border p-0  " + getDayColor(new Date(currentDate.getFullYear(), currentDate.getMonth(), day))}
                                        onMouseEnter={crossingColorChangeAdd}
                                        onMouseLeave={crossingColorChangeRemove}
                                        onClick={() => {
                                            const spanValue = document.getElementById("span" + day + issue.id)?.textContent;
                                            setDataForTimelog(issue, day);
                                            console.log("i click on " + issue.title + " and day " + day)
                                            if (spanValue) {
                                                openDeleteModal();
                                            } else {
                                                openAddModal();
                                            }
                                        }}>
                                        <div className={"flex justify-center items-center"}>
                                        <span id={"span" + day + issue.id}>
                                            {getTimeForIssueAndDay(issue.id, day, currentDate)}
                                        </span>
                                            <BsPlusLg className={"hidden"}/>
                                        </div>
                                    </td>
                                ))}
                                <td className="border px-4 py-2">{getTotalTimeForIssue(issue.id)}</td>
                            </tr>
                        ))}
                        <tr className="text-center font-bold">
                            <td colSpan={daysInMonth + 2}
                                title={formatTime(getTotalTimeForMonth())}
                                className="border px-4 py-2 text-left">MND: {getTotalTimeForMonthInManDays()}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            ) : (
                <LoadingComponent/>
            )}
            <div>
                <button onClick={() => downloadTableAsCSV()} className={actionButtonStyle}>Download CSV
                </button>
            </div>
            {!issuesLoading && <AddTimeModal onClose={closeAddModal}
                                             isOpen={isAddModalOpen}
                                             inputIssues={issues}
                                             projects={projects}
                                             currentUser={currentUser as User}
                                             inputDate={chosenDate}
                                             inputIssueId={chosenIssue?.id as string}/>}
            {!issuesLoading && <LogsTableModal isOpen={isDeleteModalOpen}
                                               onClose={closeDeleteModal}
                                               openAddModal={openAddModal}
                                               inputTimeLogs={chosenIssue?.timelogs.nodes?.filter(function (log) {
                                                   return format(new Date(log?.spentAt), 'yyyy-MM-dd') === format(chosenDate, 'yyyy-MM-dd')
                                               }) as Timelog[]}
                                               issueTitle={chosenIssue?.title as string}/>}
        </div>
    );
}

export default TimesheetTable;