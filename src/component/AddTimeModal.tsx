import React, {useEffect, useState} from "react";
import {Issue, Project, TimelogCreateInput, User} from "../gql model/graphql";
import {useMutation} from "@apollo/client";
import {ALL_ISSUES, CREATE_TIMELOG} from "../services/queries";
import {format} from 'date-fns';

interface AddTimeModalProps {
    isOpen: boolean;
    onClose: () => void;
    inputIssues: Issue[];
    projects: Project[]
    currentUser: User;
    inputDate: Date;
    inputIssueId: string;
}

export function AddTimeModal({isOpen, onClose, inputIssues, projects, currentUser, inputDate, inputIssueId}: AddTimeModalProps) {

    const [selectedIssueId, setSelectedIssueId] = useState(inputIssueId);
    const [issues, setIssues] = useState<Issue[] | null>(null);
    const [selectedProjectId, setSelectedProjectId] = useState('')
    const [errorMinutes, setErrorMinutes] = useState('');
    const [errorHours, setErrorHours] = useState('');
    const [errorDate, setErrorDate] = useState('');
    const [errorSummary, setErrorSummary] = useState('');
    const [hours, setHours] = useState("");
    const [minutes, setMinutes] = useState("");
    const [summary, setSummary] = useState("");
    const [spentAt, setSpentAt] = useState(inputDate);
    const [createTimelog] = useMutation(CREATE_TIMELOG, {
        refetchQueries: [ALL_ISSUES]
    });

    const numberInputStyle = "shadow appearance-none border rounded w-14 my-2 mx-4 px-2 text-gray-700 text-right "

    useEffect(() => {
        if (!isOpen) {
            setErrorMinutes('');
            setErrorHours('');
            setErrorDate('');
            setErrorSummary('');
            setHours('');
            setMinutes('');
            setSummary('');
        }
    }, [isOpen]);

    useEffect(
        () => setSpentAt(inputDate),
        [inputDate]
    );

    useEffect(
        () => {
            setSelectedIssueId(inputIssueId);
            console.log(selectedIssueId);
            const selectedIssue = inputIssues.find(issue => issue.id === inputIssueId);
            if (selectedIssue) {
                const projectId = "gid://gitlab/Project/" + selectedIssue.projectId
                setSelectedProjectId(projectId);
                filterIssuesByProject(projectId);
            }
        },
        [inputIssueId]
    );

    // useEffect(
    //     () => {
    //         filterIssuesByProject(selectedProjectId);
    //     },
    //     [selectedProjectId]
    // );

    useEffect(
        () => {
            setIssues(inputIssues)
        },
        [inputIssues]
    )

    /**
     * A handler function for choosing an issue.
     *
     * @param e - The change event
     */
    const chooseIssueHandler = function (e: React.ChangeEvent<HTMLSelectElement>) {
        const id = e.target.value;
        const chosenIssueId = inputIssues.filter(issue => issue.id === id)[0].id;
        setSelectedIssueId(chosenIssueId);
    }

    const chooseProjectHandler = function (e: React.ChangeEvent<HTMLSelectElement>) {
        const id = e.target.value;
        const chosenProjectId = projects.find(project => project.id === id)?.id;
        // console.log(typeof chosenProjectId);
        // console.log("chosenProjectId " + chosenProjectId);
        // console.log("project id in handler before set " + selectedProjectId);
        if (chosenProjectId) {
            setSelectedProjectId(chosenProjectId);

            // // setSelectedProjectId("666");
            // console.log("project id in handler after set " + selectedProjectId);
            // console.log(projects.filter(issue => issue.id === chosenProjectId)[0].name);
            // console.log()
            filterIssuesByProject(chosenProjectId);
            if (issues) {
                console.log(issues[0].title);
                setSelectedIssueId(issues[0].id);
            }
            // console.log(inputIssues.filter(issue => issue.id === selectedIssueId)[0].title);
        }
    }

    const filterIssuesByProject = (chosenProjectId: string) => {
        debugger
        const tmp = inputIssues.filter(function (issue){
            console.log('project id ' + chosenProjectId.split('/').pop());
            console.log('issue belongs to project id ' + issue.projectId);
            if(issue.projectId + "" === chosenProjectId.split('/').pop()){
                return true
            }else{
                return false
            }

        });
        console.log(tmp)
        setIssues(tmp);
        console.log("after")
        console.log(issues)
    };

    /**
     * A function for a form which creates new timelog
     *
     * @param e - The form event.
     */
    const submitHandler = function (e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault()
        try {
            if (selectedIssueId && hours && minutes && spentAt) {
                console.log("i send to query this id " + inputIssues.filter(issue => issue.id === selectedIssueId)[0].title);
                const timelogCreateInput: TimelogCreateInput = {
                    clientMutationId: currentUser.id,
                    issuableId: selectedIssueId,
                    spentAt: spentAt.toISOString(),
                    summary: summary,
                    timeSpent: hours + "h " + minutes + "m"
                }
                createTimelog({variables: {input: timelogCreateInput}}).catch(error => console.log(error))
                onClose();
            }
        } catch (error) {
            console.log(error)
        }

    }

    /**
     * A function for validating a date.
     *
     * @param e - The change event
     */
    const dateValidation = function (e: React.ChangeEvent<HTMLInputElement>) {
        const dateStr = e.target.value;
        const date = new Date(dateStr);
        date.setHours(0, 0, 0);
        if (date > new Date()) {
            setErrorDate("You cannot choose a date from the future");
        } else if (date < new Date(2011, 10, 13)) {
            setErrorDate("GitLab did not exist yet");
        } else {
            setSpentAt(date);
            setErrorDate('');
        }
    }

    /**
     * A function for validating a summary.
     *
     * @param e - The change event
     */
    const summaryValidation = function (e: React.ChangeEvent<HTMLTextAreaElement>) {
        const sum = e.target.value;
        if (sum.length > 100) {
            setErrorSummary("Max 100 characters");
        } else {
            setErrorSummary('');
            setSummary(sum);
        }
    }

    /**
     * A function for validating hours.
     *
     * @param e - The change event
     */
    const hoursValidation = function (e: React.ChangeEvent<HTMLInputElement>) {
        const h = e.target.value;
        const intHour = parseInt(h);
        if (intHour > 8 || intHour < 0) {
            setErrorHours("The hour value must be greater than 8 and less than 0")
        } else {
            setErrorHours('');
            setHours(h);
        }
    }

    /**
     * A function for validating minutes.
     *
     * @param e - The change event
     */
    const minutesValidation = function (e: React.ChangeEvent<HTMLInputElement>) {
        const m = e.target.value;
        const intMin = parseInt(m);
        if (intMin > 59 || intMin < 0) {
            setErrorMinutes("The minute value must be greater than 59 and less than 0")
        } else {
            setErrorMinutes('');
            setMinutes(m);
        }
    }


    return (
        <div className={isOpen ? "" : "hidden"}>
            <div
                className="fixed bg-black/25 top-0 right-0 left-0 bottom-0 "
                onClick={onClose}
            />
            <div
                className="w-[500px] p-5 rounded bg-white fixed top-10 left-1/2 -translate-x-1/2"
            >
                <h1 className='text-2xl text-center mb-2'>Add Timelog</h1>
                <form onSubmit={e => submitHandler(e)}>
                    <h1>Projects:</h1>
                    <select name="projects" value={selectedProjectId}
                            onChange={(e) => {
                                chooseProjectHandler(e)
                            }}
                            className="border py-2 px-4 mb-2 w-full shadow rounded">
                        {projects.map((project) => (
                            <option key={project.id} value={project.id}
                                    className="">{project.name}</option>
                        ))}
                    </select>
                    <h1>Issues:</h1>
                    <select name="issues" value={selectedIssueId}
                            onChange={(e) => {
                                chooseIssueHandler(e)
                            }}
                            className="border py-2 px-4 mb-2 w-full shadow rounded">
                        {issues && issues.map((issue) => (
                            <option key={issue.id} value={issue.id}
                                    className="">{issue.title}</option>
                        ))}
                    </select>
                    <div className="flex flex-col items-center">
                        <div className="flex items-center justify-center mt-4">
                            <input id="h" type="number"
                                   value={hours}
                                   onChange={e => hoursValidation(e)}
                                   className={`${numberInputStyle} ${errorHours ? 'border-red-500' : ''}`}/>
                            <label htmlFor="h" className="py-2">Hours</label>
                            <input id="m" type="number"
                                   value={minutes}
                                   onChange={e => minutesValidation(e)}
                                   className={`${numberInputStyle} ${errorMinutes ? 'border-red-500' : ''}`}/>
                            <label htmlFor="m" className="py-2">Minutes</label>
                        </div>
                        {errorHours && <span className="text-red-500">{errorHours}</span>}
                        {errorMinutes && <span className="text-red-500">{errorMinutes}</span>}
                    </div>
                    <div className="flex flex-col items-center">
                        <div className="flex items-center justify-center mt-4">
                            <label htmlFor="spent-at">
                                Spent at
                                <span className="text-gray-500"> (optional)</span>
                            </label>
                            <input id="spent-at" type="date" value={format(spentAt, 'yyyy-MM-dd')}
                                   onChange={e => dateValidation(e)}
                                   className={`shadow appearance-none border rounded px-2 my-2 mx-4 text-gray-700 ${errorDate ? 'border-red-500' : ''}`}/>
                        </div>
                        {errorDate && <span className="text-red-500">{errorDate}</span>}
                    </div>
                    <div className="mt-4 flex flex-col items-center">
                        <label htmlFor="summary" className="">
                            Summary
                            <span className="text-gray-500"> (optional)</span>
                        </label>
                        <textarea value={summary} id="summary" rows={4}
                                  onChange={e => summaryValidation(e)}
                                  className={`resize-none appearance-none shadow border w-1/2 rounded px-2 my-2 mx-4 text-gray-700 ${errorSummary ? 'border-red-500' : ''}`}></textarea>
                        {errorSummary && <span className="text-red-500">{errorSummary}</span>}
                    </div>
                    <div className="flex items-center justify-center mt-4">
                        <button className="w-1/4 px-4 py-2 m-3 rounded border bg-blue-500 text-white"
                                type='submit'>
                            Save
                        </button>
                        <button onClick={() => {
                            onClose()
                        }}
                                className="w-1/4 px-4 py-2 m-3 rounded border">
                            Cancel
                        </button>
                    </div>
                </form>
            </div>
        </div>
    )
}
