import React, {useEffect, useState} from "react";
import {Timelog, TimelogDeleteInput} from "../gql model/graphql";
import {AiOutlineDelete} from "react-icons/ai";
import {formatTime} from "../utils/DateTimeUtils";
import {format} from "date-fns";
import {DELETE_TIMELOG, ALL_ISSUES} from "../services/queries";
import {useMutation} from "@apollo/client";

interface LogsTableModalProps {
    isOpen: boolean;
    onClose: () => void;
    inputTimeLogs : Timelog[];
    issueTitle: string;
    openAddModal: () => void
}

export function LogsTableModal({onClose, isOpen, openAddModal, inputTimeLogs, issueTitle}: LogsTableModalProps) {

    const [timeLogs, setTimeLogs] = useState(inputTimeLogs);
    const [deleteTimelog] = useMutation(DELETE_TIMELOG, {
        refetchQueries: [ALL_ISSUES]
    });

    useEffect(
        () => {setTimeLogs(inputTimeLogs)},
        [inputTimeLogs]
    );

    const moveToOpenAddModal = () => {
        onClose();
        openAddModal();
    }

    const deleteTimelogHandler = async (event: React.MouseEvent<HTMLButtonElement>) => {
        try {
            const timelogId = event.currentTarget.parentElement?.parentElement?.getAttribute('data-key') ?? '';
            const timelogDeleteInput: TimelogDeleteInput = {
                id: timelogId
            };

            // Perform the deletion mutation
            await deleteTimelog({ variables: { input: timelogDeleteInput } });

            // Update the state to remove the deleted timelog
            setTimeLogs(prevTimeLogs => prevTimeLogs.filter(timelog => timelog.id !== timelogId));
        } catch (error) {
            console.error(error);
        }
    };

    return (
        <div className={isOpen ? "" : "hidden"}>
            <div
                className="fixed bg-black/25 top-0 right-0 left-0 bottom-0"
                onClick={onClose}
            />
            <div className="w-[500px] p-5 rounded bg-white fixed top-14 left-1/2 -translate-x-1/2">
                <div className="flex justify-center mb-4">
                    <h1 className="font-bold">{issueTitle}</h1>
                </div>
                <table className="w-full mx-auto">
                    <thead>
                    <tr>
                        <th>Spent At:</th>
                        <th>Summary:</th>
                        <th>Time Spent:</th>
                        <th>Action:</th>
                    </tr>
                    </thead>
                        <tbody>
                        {timeLogs && timeLogs.map(timelog => (
                            <tr key={timelog.id} data-key={timelog.id} className="border-t-2 mt-2">
                                <td className="text-center">{format(new Date(timelog.spentAt), 'dd/MM/yyyy')}</td>
                                <td className="text-center">{timelog.summary}</td>
                                <td className="text-center">{formatTime(timelog.timeSpent)}</td>
                                <td className="flex justify-center items-center">
                                    <button className='rounded-md bg-red-300 w-full flex justify-center' type={"button"}
                                            onClick={(e) => deleteTimelogHandler(e)}>
                                    <AiOutlineDelete className="h-6"/>
                                    </button>
                                </td>
                            </tr>
                        ))}
                        </tbody>
                </table>
                <div className="flex items-center justify-center mt-4">
                    <button className="w-1/3 px-4 py-2 m-3 rounded border bg-blue-500 text-white"
                            onClick={moveToOpenAddModal}>
                        Add Timelog
                    </button>
                    <button onClick={() => {onClose()}}
                            className="w-1/3 px-4 py-2 m-3 rounded border">
                        Cancel
                    </button>
                </div>
            </div>
        </div>

    )
}