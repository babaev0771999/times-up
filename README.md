# Getting Started with TimesUp

Welcome to TimesUp, a React app designed to track and manage your time effectively. This app allows you to log in using your GitLab API token and provides various features for time management.

To run the app locally, execute the following command:
### `npm intall`
### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

Deployed version on link kirillbabaev.github.io

## Login

To log in to TimesUp, you need to provide your GitLab API token.

Follow the steps below to obtain your GitLab API token:

1. Visit GitLab and log in to your account.
2. Navigate to your account settings by clicking on your profile picture in the top-right corner and selecting Preferences.
3. In the left sidebar, select Access Tokens.
4. Choose a name for your token, optionally specify an expiry date, and select the desired scopes for the token. For TimesUp, the token needs the api scope.
5. Click on the Create personal access token button.
6. GitLab will generate an API token for you. Copy this token and keep it secure.
7. In the TimesUp login page, there is a single input field. Paste your GitLab API token into this field. 
8. Click on the Login button.


If the provided API token is valid, you will be successfully logged in to TimesUp.
